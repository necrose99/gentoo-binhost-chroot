## superjaded's Gentoo Binhost Chroot Process (documentation in progress)


I run Gentoo on several personal machines and rather than compiling packages on each individual system, I use Gentoo's binary package system to build packages on my personl server and then consume them on those other machines.

I've developed the below system to streamline the process to some extent.

### The scripts and /etc/portage setup

#### start\_chroot.sh

For those who have done a traditional install of Gentoo, what this script does should look vaguely familiar. This
script will "start" a chroot, which basically means that it will set a chroot up (including all the
various bind mountpoints that are needed) and then chroot the user into that chroot path. Once you leave the chroot, it will automatically attempt to umount everything that it mounted.

This script will be needed on the computer where the binary host is being setup.


#### portagedir-sync.sh

Meant to be used on the clients to sync any changes made in /etc/portage on the binhost to
the client from which this script is run.

This script is needed on the clients who are going to be consuming binaries from the binhost.

#### etc.portage

The /etc/portage used for my binhost to illustrate how I've setup my /etc/portage so it's a bit
easier to keep everything in sync.

Most of the files in this folder is more or less the same as a traditional /etc/portage with a few
caveats.

##### Files / folders of note

* make.conf: Main thing to notice is that most of the configuration that's normally found in make.conf are sourced from external files.
* source-files: The folder where I place the files that are sourced from make.conf.

The idea is that the files under source-files (and the package.\* directories) are kept in sync via 
portagedir-sync.sh while make.conf is not kept in sync. This allows us to be more flexible on the various
systems involved if it ends up being necessary. 

### How Do I Do This?

#### Set up your Chroot

The concept of the binhost chroot is that you pretend like you're installing Gentoo to your chroot, but
the only part of the install you're really doing are the package installs. You don't need to set anything
up with respect to locale, timezone, etc in my experience. You'll want to set your profile to wahtever
your "final" profile will be before you start generating binaries since 

    syntax: ./start-chroot.sh <prefix>

At the beginning of the script you'll notice there's a CHROOT\_BASE variable set which takes a parameter
currently. I did it this way because I maintain multiple chroots with different configurations. You could take that $1 out and have CHROOT\_BASE be an explicit path if you only have interest in managing one chroot.

The start-chroot.sh script will try to set up a sane environment even if it's the first time running
the script, so it will do some sanity checking to make sure everything is set up correctly. You will 
probably end up needing to edit this script to match how you have your environment setup. For example,
/mnt/bigdata generally is my NAS running on spinning rust, so I use bind mounts for both /var/tmp/portage
and /var/notmpfs to take advantage of my tmpfs and SSD mounted there for actual compilation. If you
don't have any equivalent directories set up, you'll want to comment these out.

Also you will need to extract a stage3 tarball to the chroot directory before going too far. If bin/bash
doesn't exist under your chroot location, it will assume a stage3 tarball hasn't been extracted there
yet and refuse to proceed with entering the chroot.

In terms of step-by-step, you'll want to do the following to setup your chroot initially:

1. Take a look at start\_chroot.sh and modify so it makes sense for your system. Comment out the mounts / checks that don't make sense for your system and make sure it will point to where ever you want your chroot to be.
2. Download your stage3 tarball and extract it to the location where you'll be managing your binhost. You'll want to extract this the same way you would during a regular Gentoo installation.
3. Run ./start\_chroot.sh to enter your new chroot.
4. Setup make.conf and /etc/portage to similarly to how I've set it up, especailly with respect to make.conf and the source-files directory. Make sure your binhost is set up properly to automatically generate binaries as packages are installed as illustrated in etc.portage/source-files/binhost-options.conf.
5. Once all that is set up, use emerge-webrsync to pull in the main gentoo repo.
6. Set your "final" profile, rebuild @world (`emerge -auDNv @world --with-bdeps=y --changed-deps`) and then start installing stuff.

#### Set up Your Client

When you're initially doing your Gentoo install on your client, you will need to make sure that
your make.conf is set up correctly on your client to start off with so you won't run into too many issues
when actually trying to use the binary packages.

It should be sufficient to copy portagedir-sync.sh and make.conf from your binhost to get this
started. Use portagedir-sync.sh to sync all the relevant files from your binary host to your client to
get any USE flags in sync with your binary host as soon as possible. You'll also want to update your make.conf
to use the "client-options" rather than the "bihost-options." This will make it so your system will
attempt to use binary packages when they are available instead of building binary packages.

During the initial setup, this won't be very different from a "normal" install except that you'll want
to setup your profile to whatever your end profile is rather than sticking with the default profile.

I originally developed my binhost process utilizing NFS, but I've since then I've transitioned to using a ssh binrepo. If all of your machines are on the same network and that is the context in which you will be doing updates, NFS will be the easiest way to set it up. Effectively you just need to mount your NFS share such that the PKGDIR on your binary host is mounted in the same PKGDIR of your client. If utilizing the NFS approach, you will probably want to make sure EMERGE_DEFAULT_OPTS on your clients does NOT include the --getbinpkg option in client-options.

I also go into some detail below on how to use binrepos using ssh. This might be a better approach if any of your clients need to be able to do updates/installs from outside of your home network.

#### Performing an Update Cycle

On the host:

1. Do start\_chroot.sh to enter your chroot
2. eix-sync or emerge --sync
3. After sync is complete, use the following command to actually do your update:

     emerge -auDNv @world --with-bdeps=y --changed-deps

The changed-deps parameter is important since binary packages are kind of finicky about only being
usable when the dependencies match, so that parameter will cause packages to be rebuilt when the
dependencies have changed. This should limit the amount that portage will complain about inconsistent
dependencies.

On the client:

1. Mount your binary packages / distfiles NFS shares (if applicable)
2. eix-sync or emerge --sync
3. Run portagedir-sync.sh if necessary
4. Run the same command that you ran on your binhost:

     emerge -auDNv @world --with-bdeps=y --changed-deps

#### Utilizing a binrepo instead of a NFS mount

If you have a system that you may want to maintain away from home, it might make sense to setup a binrepo rather than relying on NFS. Whenever latency is present, NFS ends up being somewhat painful to use.

Below, I describe how I setup a binrepo using ssh. The user account I created for it was called "pkguser," so any references to pkguser below should be whatever user account you setup on your host. Gentoo should also support things like https which would work without having to worry about authentication, but I didn't feel like setting up a web server just for binrepos.

On each client:

1. On your root account, create a new ssh key with a non-default filename. I named the key "binpkg." Do not set a passphrase on the key.
2. Edit ~/.ssh/config and add the following lines so ssh presents that new IdentityFile when you login as your portage user:

    Match User pkguser
        IdentityFile ~/.ssh/binpkg

3. Make sure that EMERGE_DEFAULT_OPTS contains --getbinpkg so it attempts to utilize the binrepo.
4. Make a note of the binpkg.pub file which you'll need to copy to the host.


Generally consider how you are going to keep binrepos.conf in sync. It might make sense to have portagedir-sync.sh sync binrepos.conf in your chroot so any changes you make to binrepos.conf will be distributed to the relevant clients.

NOTE: If you use emerge -pv a lot from your regular user account often from your clients, you can do the same thing with the public key of your normal ssh key to the host as well. This way when you do emerge -pv, you won't get a password prompt even if you have a passphrase as long as you have the key loaded in your ssh agent.

On the binary host:

1. Create a new user account that portage will use to access your binary host's portage directory. I added the account to the portage group.
2. Add the public key to ~/.ssh/authorized_keys either with a text editor or ssh-copy-id from the client


It might also be worth mentioning that I set this up primarily to be able to manage systems when I'm outside of my home. You'll notice in my example binrepos.conf file I connect to "rakka" or "haibane." I've setup wireguard on my server so I can connect to my server as though it was on the local network.
